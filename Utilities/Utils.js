﻿'use strict';


Array.prototype.extend = function (other_array) {
    other_array.forEach(function (v) { this.push(v) }, this);
}