﻿'use strict';

/**
 * Service du module "Git Encouragement"
 */
app.service('RepoGitHubService', ['$http', '$q', function ($http, $q) {

	var Clienid_clientSecret;
	var AllRepos = [];
	var AllLang = [];
	//var endDate;

	//Constructors
	this.Initialize = function () {
	    //console.log("Initialize RepoGitHubService");
	}

	this.Initialize = function (clientid_clientSecret) {
	    //console.log("Initialize RepoGitHubService");
		Clienid_clientSecret = clientid_clientSecret;
	}

	//Functions Public
	this.GetRepos = function (user, repos) {
		var defer = $q.defer();
		AllRepos = [];
		return getReposUrl("https://api.github.com/users/" + user + "/repos?" + Clienid_clientSecret + "&per_page=100").then(function () {
	    	repos.extend(AllRepos);
	    	defer.resolve(); 
	    	//return AllRepos; 
	    });
	    return defer.promise;
	}

	this.GetLanguages = function (user, repo, repoLanguages) {
		AllLang = [];
		return getLanguagesUrl("https://api.github.com/repos/" + user + "/" + repo + "/languages?" + Clienid_clientSecret).then(function () {
	    	repoLanguages.extend(AllLang);

	    	return AllLang; 
	    });
	}

	//Functions Private
	var getReposUrl = function (url) {
		var defer = $q.defer();

		var loop = function (page, len) {

			// Example of a promise to wait for
			$http.get(url + "&page=" + page).then(function (response, status) {
				len = response.data.length;
				AllRepos.extend(response.data);
				//console.log(response.data);

			    // Resolve or continue with loop

				if (len < 100 ) {

					defer.resolve();
				} else {
					loop(++page);
				}
			})
		}

		loop(1); // Start loop
		return defer.promise;
	}

	var getLanguagesUrl = function (url) {
		var defer = $q.defer();

		var loop = function () {

			// Example of a promise to wait for
			$http.get(url).then(function (response, status) {
				var langs = [];
				//console.log("l'objet' " + response.data);
				for(var lang in response.data) {
					langs.push(lang);
					
				}

				AllLang = langs;

				defer.resolve();
			})
		}

		loop(); // Start loop
		return defer.promise;
	}

}]);


