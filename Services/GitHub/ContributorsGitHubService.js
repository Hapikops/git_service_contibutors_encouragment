﻿'use strict';

/**
 * Service du module "Git Encouragement"
 */
app.service('ContributorsGitHubService', ['$http', '$q', function ($http, $q) {

	var AllContributors = [];
	var Clienid_clientSecret;
	var TotalPullRequestsByUser;
	var DateLastPullRequestsByUser;
	var MaxTotalPullRequestsByUser = 0;
	var AcceptedPullRequestsByUser;
	var OkToContinue = false;
	

	//Constructors
	this.Initialize = function () {
		//console.log("Initialize ContributorsGitHubService");
	}

	this.Initialize = function (clientid_clientSecret)
	{
		//console.log("Initialize ContributorsGitHubService");
		Clienid_clientSecret = clientid_clientSecret;
	}

	//Functions Public
	this.GetContributors = function (user, repo, contributors) {
	    var defer = $q.defer();
		OkToContinue = false;
		TotalPullRequestsByUser = {};
		DateLastPullRequestsByUser = {};
		AcceptedPullRequestsByUser = {};
		MaxTotalPullRequestsByUser = 0;
		AllContributors = contributors;
		GetContributorsUrl("https://api.github.com/repos/" + user + "/" + repo + "/contributors?" + Clienid_clientSecret + "&per_page=100").then(function (result) {
		    CompleteContributorsInfo(AllContributors).then(function () {
		        AllContributors.sort(function (a, b) {
					if (a.ratio > b.ratio)
						return -1;
					if (a.ratio < b.ratio)
						return 1;
					// a doit être égale à b
					return 0;
		        });
				defer.resolve();
			});
		});

		return defer.promise;
	}

	this.GetUser = function (login) {
		return GetUserUrl("https://api.github.com/users/" + login + "?" + Clienid_clientSecret).then(function (result) { return AllContributors; });;
	}

	this.GetContributionsByUser = function (AllPullRequests) {
	    for (var item in AllPullRequests) {
	        if (AllPullRequests[item].hasOwnProperty("user")) {
	            var login = AllPullRequests[item].user.login;
	            if (TotalPullRequestsByUser.hasOwnProperty(login)) {
	                TotalPullRequestsByUser[login]++;
	            }
	            else
	                TotalPullRequestsByUser[login] = 1;

	            if (!DateLastPullRequestsByUser.hasOwnProperty(login)) {
	                DateLastPullRequestsByUser[login] = AllPullRequests[item].closed_at;
	            }

	            if (AllPullRequests[item].merged_at != null) {
	                if (AcceptedPullRequestsByUser.hasOwnProperty(login))
	                    AcceptedPullRequestsByUser[login]++;
	                else
	                    AcceptedPullRequestsByUser[login] = 1;
	            }
	        }
	    }


	    for (var truc in TotalPullRequestsByUser) {
	        if (MaxTotalPullRequestsByUser < TotalPullRequestsByUser[truc])
	            MaxTotalPullRequestsByUser = TotalPullRequestsByUser[truc];
	    }

	    OkToContinue = true;
	}

	//Functions Private
	var GetContributorsUrl = function (url) {
	    var defer = $q.defer();
	    var loop = function (page, len) {
		    $http.get(url + "&page=" + page).then(function (response, status) {
		        len = response.data.length;
		        AllContributors.extend(response.data);
				if (len < 100)
					defer.resolve();
				else
					loop(++page);
			});

		}

		loop(1); // Start loop
		return defer.promise;
	}

	var CompleteContributorsInfo = function (Contributors) {
	    var ok = false
	    var count2 = 0;
		var defer = $q.defer();
		var loop = function (count) {
		    count2++;
			var item = Contributors[count];
			if (count <= Contributors.length - 1) {
			    GetUser(item.login).then(function (user) {
					item.email = user.email;
					var reallyOkToContinue = false;
					var loop2 = function ()
					{
					    if (AcceptedPullRequestsByUser[item.login] != null)
					        item.pullRequestAccepted = AcceptedPullRequestsByUser[item.login];
					    else
					        item.pullRequestAccepted = 0
					    if (TotalPullRequestsByUser[item.login] != null)
					        item.pullRequest = TotalPullRequestsByUser[item.login];
					    else
					        item.pullRequest = 0;

					    item.ratio = (item.pullRequestAccepted / item.pullRequest).toFixed(2);

					    if (TotalPullRequestsByUser[item.login] != 0 && Math.log(MaxTotalPullRequestsByUser) != 0) 
					        item.ratioLog = (Math.log(TotalPullRequestsByUser[item.login]) / Math.log(MaxTotalPullRequestsByUser)).toFixed(2);
					    else
					        item.ratioLog = NaN;

					    item.ratioRevised = (item.ratio * item.ratioLog).toFixed(2);

					    item.dateLastPullRequests = DateLastPullRequestsByUser[item.login];

					    item[item.login] = item.login;

					    // This game with the setTimeout allow to wait that the funtion GetContributionsByUser was finished, before to complete the informations
						if (!OkToContinue)
							setTimeout(loop2, 1);
						else if (OkToContinue && reallyOkToContinue) {
						    count2--;
						    if (count2 == 1) {
						        defer.resolve();
						    }
						}
						else {
						    reallyOkToContinue = true;
						    setTimeout(loop2, 3000);
						}
						
					}
					loop2();
				});
				loop(++count);
			}
		}
		loop(0); // Start loop

		return defer.promise;
	}

	var GetUser = function (login) {
		return GetUserUrl("https://api.github.com/users/" + login + "?" + Clienid_clientSecret).then(function (result) { return result; });;
	}

	var GetUserUrl = function (url) {
		var promise = $http.get(url).then(function (response) {
			var data = response.data;
			return data;
		});
		return promise;
	}

}]);

