﻿'use strict';

/**
 * Service du module "Git Encouragement"
 */
app.service('PullRequestGitHubService', ['$http', '$q', function ($http, $q) {

	var Clienid_clientSecret;
	var AllPullRequests = [];
	var EndDate;

	//Constructors
	this.Initialize = function () {
		//console.log("Initialize PullRequestGitHubService");
	}

	this.Initialize = function (clientid_clientSecret) {
		//console.log("Initialize PullRequestGitHubService");
		Clienid_clientSecret = clientid_clientSecret;
	}

	//Functions Public
	this.GetPullRequests = function (user, repo, endDate) {
	    EndDate = endDate;
	    AllPullRequests = [];
		return getPullRequestsUrl("https://api.github.com/repos/" + user + "/" + repo + "/pulls?state=closed&" + Clienid_clientSecret + "&per_page=100").then(function () { return AllPullRequests; });
	}

	//Functions Private
	var getPullRequestsUrl = function (url) {
	    var defer = $q.defer();


		var loop = function (page, len) {

			// Example of a promise to wait for
			$http.get(url + "&page=" + page).then(function (response, status) {
				len = response.data.length;
				AllPullRequests.extend(response.data);

			    // Resolve or continue with loop
				if (AllPullRequests.length != 0) {

				    var datestring = AllPullRequests[AllPullRequests.length - 1].closed_at;
				    var regex = /(....)-(..)-(..)T/g;
				    var res = regex.exec(datestring);
				    var date = moment(res[1] + "-" + res[2] + "-" + res[3]);

				    //console.log(datestring + "   --- < ---   " + EndDate.toISOString());

				    // moment().subtract(1, 'years')
				    if (len < 100 || date < EndDate) {

				        var allPRLength = AllPullRequests.length - 1;
				        // Remove the PR of the last page that exceeding the date limit
				        for (var i = 0; i < len; i++) {

				            var indice = allPRLength - i;
				            var datestring = AllPullRequests[indice].closed_at;

				            var regex = /(....)-(..)-(..)T/g;
				            var res = regex.exec(datestring);
				            var date = moment(res[1] + "-" + res[2] + "-" + res[3]);

				            if (date < EndDate)
				                AllPullRequests.splice(indice, 1);
				        }

				        defer.resolve();
				    } else {

				        loop(++page);
				    }
				}
				else {

				    defer.resolve();
				}
			})
		}

		loop(1); // Start loop
		return defer.promise;
	}

}]);


