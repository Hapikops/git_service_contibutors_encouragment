﻿'use strict';

/**
 * Service du module "Git Encouragement"
 */
app.service('RecommendationService', ['$http', '$q', 'RepoGitHubService', function ($http, $q, RepoGitHubService) {

	var Clienid_clientSecret;
	var AllRepos = [];
	var AllLang = [];
	//var endDate;

	//Constructors
	this.Initialize = function () {
		//console.log("Initialize RecommendationService");
	}

	this.Initialize = function (clienid_clientSecret) {
		//console.log("Initialize PullRequestGitHubServiceRecommendationService");
		Clienid_clientSecret = clienid_clientSecret;
		RepoGitHubService.Initialize(clienid_clientSecret);
	}

	//Functions Public

	/*
	Return the contributor with the list of 
	*/
	this.SetMatchedRepos = function (visitorLogin, contributor, based_repo_name) {
		//DateLimit = dateLimit;
		var defer = $q.defer();

		var visitor_repos = [];
		var languageCounter = [];
		var favorite_lang = "";

		var contributor_login = contributor.login;
		var contributor_repos = [];
		var matched_repos = [];

		var todays_date = moment();
		var aMonth = moment("2016-10-30") - moment("2016-10-01");
		var finish = false;

		contributor.hasRepo = false;
		RepoGitHubService.GetRepos(visitorLogin, visitor_repos).then(function () {
			
			languageCounter = SetLanguageCounter(visitor_repos);
			//console.log("Service " + contributor_login);
			favorite_lang = GetFavoriteLanguage(visitor_repos);

			RepoGitHubService.GetRepos(contributor_login, contributor_repos).then(function () {
				for(var i in contributor_repos) {

					if(contributor_repos[i].language == favorite_lang) {
						if((contributor_repos[i].pushed_at != null) && (contributor_repos[i].private == false)) {
							var datestring = contributor_repos[i].pushed_at;
							var regex = /(....)-(..)-(..)T/g;
							var res = regex.exec(datestring);
							var date = moment(res[1] + "-" + res[2] + "-" + res[3]);

							if((todays_date - date) <= aMonth) {
								if (contributor_repos[i].name != based_repo_name) {
									matched_repos.push(contributor_repos[i]);
									contributor.hasRepo = true;
									//console.log(contributor_login + "     " + contributor_repos[i].name);
								}
							}
						}
					}
				}

				contributor.repo1 = matched_repos[0];
				contributor.repo2 = matched_repos[1];
				contributor.repo3 = matched_repos[2];

				finish = true;
				
			});
			var loop2 = function () {
				if (!finish) {
					setTimeout(loop2, 50);
				}
				else {
					defer.resolve();
				}
			}
			loop2();
		});

		return defer.promise;
	}

	this.GetFavoriteLanguage = function (repos) {
	    return GetFavoriteLanguage(repos);
	}

	var GetFavoriteLanguage = function (repos) {

	    var languageCounter = SetLanguageCounter(repos);
	    var cpt = 0;
	    var lang = "";
	    for (var l in languageCounter) {
	        if (l != null && languageCounter[l] > cpt) {
	            cpt = languageCounter[l];
	            lang = l;
	        }
	    }
	    return lang;
	}

	var SetLanguageCounter = function (repos) {
	    var languageCounter = new Object();
	    for (var i = 0; i < repos.length; i++) {
	        var repoLanguages = [];

	        if (languageCounter.hasOwnProperty(repos[i].language))
	            languageCounter[repos[i].language]++;
	        else
	            languageCounter[repos[i].language] = 1;
	    }

	    return languageCounter;
	}



	var GetReposWithThisLang = function (repos, language, nbRepo) {
	    var selectedRepos = [];
	    var foundRepo = 0;
	    for (var i = 0; i < repos.length && foundRepo < nbRepo; i++) {
	        if (repos[i].language == language) {
	            selectedRepos.push(repos[i].name);
	            foundRepo++;
	        }

	    }
	    return selectedRepos;
	}
	

}]);


