﻿'use strict';

/**
 * Service du module "Git Encouragement"
 */
app.service('GitHubService', ['ContributorsGitHubService', '$q', 'PullRequestGitHubService', 'RepoGitHubService', 'RecommendationService', function (ContributorsGitHubService, $q, PullRequestGitHubService, RepoGitHubService, RecommendationService) {

	var clientId = "37d2bc2f5ecfc0b9a865";
	var clientSecret = "08649c9493b62f151a1497173aeee28e29055b49";
	var clientid_clientSecret = "client_id=" + clientId + "&client_secret=" + clientSecret;

	

	//Constructors
	this.Initialize = function () {
		//console.log("Initialize GitHubService");
		ContributorsGitHubService.Initialize(clientid_clientSecret);
		PullRequestGitHubService.Initialize(clientid_clientSecret);
		RecommendationService.Initialize(clientid_clientSecret);
		RepoGitHubService.Initialize(clientid_clientSecret);
	}
	
	//Fucntions Public
	this.GetContributors = function (user, repo, contributors, endDateMoment) {
		var defer = $q.defer()
		ContributorsGitHubService.GetContributors(user, repo, contributors).then(function () { defer.resolve(); });
		PullRequestGitHubService.GetPullRequests(user, repo, endDateMoment).then(function (result) { ContributorsGitHubService.GetContributionsByUser(result); });
		return defer.promise;
	}

	this.GetContributors = function (user, repo, contributors, startDateMoment, endDateMoment) {
		var defer = $q.defer()
		var Contributors = contributors;
		ContributorsGitHubService.GetContributors(user, repo, contributors).then(function () { KeepContributorBetween(Contributors, startDateMoment, endDateMoment), defer.resolve(); });
		PullRequestGitHubService.GetPullRequests(user, repo, endDateMoment).then(function (result) { ContributorsGitHubService.GetContributionsByUser(result); });
		return defer.promise;
	}


	this.GetRepos = function (user, repos) {
		var defer = $q.defer();
		RepoGitHubService.GetRepos(user, repos).then(function () { defer.resolve(); });
		return defer.promise;
	}

	this.SetMatchedRepos = function (visitorLogin, aContributor, aBasedRepoName) {
		var defer = $q.defer();
		RecommendationService.SetMatchedRepos(visitorLogin, aContributor, aBasedRepoName).then(function () {
		    defer.resolve();
		});
		return defer.promise;
	}


	this.GetFavoriteLanguage = function (userRepos) {
		return RecommendationService.GetFavoriteLanguage(userRepos);
	}


	//Functions Private
	/*
	* Start date is for exemple 04-09-2016
	* End date is for exemple 04-08-2016
	* With this exemple you will keep the contributor who have made pull request between 04-08-2016 to 04-09-2016 
	* The startDateMoment must be greater then the endDateMoment
	*/
	var KeepContributorBetween = function (Contributors, startDateMoment, endDateMoment) {

		//Remove the contributor who have made a pull request before the startDateMoment
		for (var i = Contributors.length - 1; i >= 0; i--) {
			var regexx = /(....)-(..)-(..)T/g;
			if (Contributors[i].dateLastPullRequests != null) {
				var ress = regexx.exec(Contributors[i].dateLastPullRequests);
				var dateLastPullRequests = moment(ress[1] + "-" + ress[2] + "-" + ress[3]);

				if (dateLastPullRequests > startDateMoment) {
					Contributors.splice(i, 1);
				}
			}
		}

		//Remove the contributor who have made a pull request after the endDateMoment
		for (var i = Contributors.length - 1; i >= 0; i--) {
			var regexx = /(....)-(..)-(..)T/g;
			if (Contributors[i].dateLastPullRequests != null) {
				var ress = regexx.exec(Contributors[i].dateLastPullRequests);
				var dateLastPullRequests = moment(ress[1] + "-" + ress[2] + "-" + ress[3]);

				if (dateLastPullRequests < endDateMoment) {
					Contributors.splice(i, 1);
				}
			}
		}
	}

}]);

