﻿'use strict';

/**
 * Contrôleur du module "Git Encouragement"
 */
app.controller('NewContributorController', ['$scope', '$http', 'GitHubService', '$q', function ($scope, $http, GitHubService, $q) {

    // Variables
    $scope.contributors = [];
    $scope.repositoryUrl;
    $scope.endDate = new Date();
    $scope.startDate = new Date();
    $scope.isDisplayNonPullRequestContributor = true;
    $scope.isLoading = false;
    $scope.visitorLogin = "";
    $scope.visitorFavoriteLanguage = "Unknown";

    var NonPullRequestContributor = [];

    GitHubService.Initialize();

    // Public Functions
    $scope.SortByRatioRevised = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.ratioRevised > b.ratioRevised)
                return -1;
            if (a.ratioRevised < b.ratioRevised)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }

    $scope.NewContributors = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;
        $scope.visitorFavoriteLanguage = "Unknown";

        var startDateMoment = moment();
        var endDateMoment = moment().subtract(3, "months");
        var startDateMoment2 = moment().subtract(3, "months");
        var endDateMoment2 = moment().subtract(12, "months");
        var newContributor = [];
        var oldContributor = [];
        var maxTotalPullRequestsByNewUser = 0;
        var visitorRepos = [];

        GitHubService.GetRepos($scope.visitorLogin, visitorRepos).then(function () {
            $scope.visitorFavoriteLanguage = GitHubService.GetFavoriteLanguage(visitorRepos);
        });

        GitHubService.GetContributors(repoOwner, repoName, newContributor, startDateMoment, endDateMoment).then(function () {
            GitHubService.GetContributors(repoOwner, repoName, oldContributor, startDateMoment2, endDateMoment2).then(function () {
                for (var i = 0; i < newContributor.length; i++) {
                    if (oldContributor[newContributor.login] != null)
                        newContributor.splice(i, 1);
                }

                $scope.contributors = newContributor;
                $scope.DisplayNonPullRequestContributor();

                for (var i = 0; i < $scope.contributors.length; i++) {
                    // Trouver le max the PR de tout les newContributor
                    if ($scope.contributors[i].pullRequest > maxTotalPullRequestsByNewUser)
                        maxTotalPullRequestsByNewUser = $scope.contributors[i].pullRequest;
                }

                for (var i = 0; i < $scope.contributors.length; i++) {
                    // Recalcul du ratio revised sur les nouveaux uniquement
                    if ($scope.contributors[i].pullRequest != 0 && Math.log(maxTotalPullRequestsByNewUser) != 0)
                        $scope.contributors[i].ratioLog = (Math.log($scope.contributors[i].pullRequest) / Math.log(maxTotalPullRequestsByNewUser)).toFixed(2);
                    else
                        $scope.contributors[i].ratioLog = NaN;
                    $scope.contributors[i].ratioRevised = ($scope.contributors[i].ratio * $scope.contributors[i].ratioLog).toFixed(2);

                    // Mettre en forme la date du dernier PR
                    $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);
                }

                SetMatchedRepos($scope.visitorLogin, $scope.contributors, repoName).then(function () {
                    $scope.SortByRatioRevised();
                    $scope.isLoading = false;
                });
            });
        });
    }

    $scope.NewContributorsRecommandation = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;
        $scope.visitorFavoriteLanguage = "Unknown";

        var startDateMoment = moment();
        var endDateMoment = moment().subtract(6, "months");
        var maxTotalPullRequestsByNewUser = 0;
        var visitorRepos = [];

        GitHubService.GetRepos($scope.visitorLogin, visitorRepos).then(function () {
            $scope.visitorFavoriteLanguage = GitHubService.GetFavoriteLanguage(visitorRepos);
        });

        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMoment).then(function () {
            $scope.DisplayNonPullRequestContributor();
            SetMatchedRepos($scope.visitorLogin, $scope.contributors, repoName).then(function () {
                $scope.SortByRatioRevised();

                var j = 0;
                for (var i = 0; i < $scope.contributors.length; i++) {
                    // Garder les 20 premiers qui ont un repo#1
                    
                    if (!$scope.contributors[i].hasRepo) {
                        $scope.contributors.splice(i, 1);
                        j--;
                        i--;
                    }
                }

                for (var i = 0; i < $scope.contributors.length; i++)
                    $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);

                $scope.isLoading = false;
            });
        });
    }

    $scope.DisplayNonPullRequestContributor = function () {
        if ($scope.isDisplayNonPullRequestContributor) {
            for (var i = $scope.contributors.length - 1; i >= 0; i--) {
                if ($scope.contributors[i].pullRequest == 0) {
                    NonPullRequestContributor.push($scope.contributors.splice(i, 1)[0]);
                }
            }
            $scope.isDisplayNonPullRequestContributor = false;
        }
        else {
            for (var i = NonPullRequestContributor.length - 1; i >= 0; i--) {
                $scope.contributors.push(NonPullRequestContributor[i]);
            }
            NonPullRequestContributor = [];
            $scope.isDisplayNonPullRequestContributor = true;
        }
    }

    $scope.DisplayDateMoment = function (dateMoment) {
        if (dateMoment != null) {
            var regex = /(....)-(..)-(..)T/g;
            var res = regex.exec(dateMoment);
            return res[1] + "/" + res[2] + "/" + res[3];
        }
    }

    //Functions Private
    var SetMatchedRepos = function (visitorLogin, contributors, repoName) {
        var defer = $q.defer();
        var loop = function (i) {
            GitHubService.SetMatchedRepos(visitorLogin, contributors[i], repoName).then(function () {
                if (i >= contributors.length - 1) {
                    defer.resolve();
                } else {
                    loop(++i);
                }
            });
        }

        loop(0); // Start loop
        return defer.promise;
    }

    // Events
    angular.element(document).ready(function () {

    });

}]);


