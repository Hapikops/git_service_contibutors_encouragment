﻿'use strict';

/**
 * Contrôleur du module "Git Encouragement"
 */
app.controller('ContributorController', ['$scope', '$http', 'RecommendationService', 'ContributorsGitHubService', 'GitHubService', '$q', function ($scope, $http, RecommendationService, ContributorsGitHubService, GitHubService, $q) {

    // Variables
    $scope.contributors = [];
    $scope.repoLanguages = [];
    $scope.repositoryUrl;
    $scope.endDate = new Date();
    $scope.startDate = new Date();
    $scope.isDisplayNonPullRequestContributor = true;
    $scope.isLoading = false;
    $scope.visitorLogin = "";
    $scope.visitorFavoriteLanguage = "Unknown";

    var NonPullRequestContributor = [];

    var clientId = "37d2bc2f5ecfc0b9a865";
    var clientSecret = "08649c9493b62f151a1497173aeee28e29055b49";
    var clientid_clientSecret = "client_id=" + clientId + "&client_secret=" + clientSecret;
    var languageCounter = new Object();

    GitHubService.Initialize(clientid_clientSecret);

    // Public Functions
    $scope.SubmitContributors = function () {

        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;
        $scope.visitorFavoriteLanguage = "Unknown";

        var startDateMoment = moment();
        var endDateMomentMonths = moment().subtract(6, 'months');
        var visitorRepos = [];

        GitHubService.GetRepos($scope.visitorLogin, visitorRepos).then(function () {
            $scope.visitorFavoriteLanguage = GitHubService.GetFavoriteLanguage(visitorRepos);
        });


        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMomentMonths).then(function () {
            $scope.DisplayNonPullRequestContributor();

            SetMatchedRepos($scope.visitorLogin, $scope.contributors, repoName).then(function () {
                $scope.SortByRatioRevised();
                $scope.isLoading = false;
            });

        });

    }

    $scope.RecommendContributors = function () {

        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;
        $scope.visitorFavoriteLanguage = "Unknown";

        var startDateMoment = moment();
        var endDateMomentYear = moment().subtract(12, 'months');
        var visitorRepos = [];

        GitHubService.GetRepos($scope.visitorLogin, visitorRepos).then(function () {
            $scope.visitorFavoriteLanguage = GitHubService.GetFavoriteLanguage(visitorRepos);
        });


        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMomentYear).then(function () {
            $scope.DisplayNonPullRequestContributor();

            SetMatchedRepos($scope.visitorLogin, $scope.contributors, repoName).then(function () {
                $scope.SortByRatioRevised();

                var j = 0;
                for (var i = 0; i < $scope.contributors.length; i++) {
                    // Garder les 20 premiers qui ont un repo#1

                    if (!$scope.contributors[i].hasRepo) {
                        $scope.contributors.splice(i, 1);
                        j--;
                        i--;
                    }
                }

                $scope.isLoading = false;
            });

        });

    }


    //Functions Private
    var SetMatchedRepos = function (visitor_login, contributors, repoName) {
        var defer = $q.defer();

        var loop = function (visitor_login, contributors, i) {

            GitHubService.SetMatchedRepos(visitor_login, contributors[i], repoName).then(function () {
                if (i >= contributors.length - 1) {
                    defer.resolve();
                } else {
                    loop(visitor_login, contributors, ++i);
                }
            });
        }

        loop(visitor_login, contributors, 0); // Start loop
        return defer.promise;
    }


    $scope.DisplayNonPullRequestContributor = function () {
        if ($scope.isDisplayNonPullRequestContributor) {
            for (var i = $scope.contributors.length - 1; i >= 0; i--) {
                if ($scope.contributors[i].pullRequest == 0) {
                    NonPullRequestContributor.push($scope.contributors.splice(i, 1)[0]);
                }
            }
            $scope.isDisplayNonPullRequestContributor = false;
        }
        else {
            for (var i = NonPullRequestContributor.length - 1; i >= 0; i--) {
                $scope.contributors.push(NonPullRequestContributor[i]);
            }
            NonPullRequestContributor = [];
            $scope.isDisplayNonPullRequestContributor = true;
        }
    }

    $scope.SortByRatio = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.ratio > b.ratio)
                return -1;
            if (a.ratio < b.ratio)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }


    $scope.SortByRatioRevised = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.ratioRevised > b.ratioRevised)
                return -1;
            if (a.ratioRevised < b.ratioRevised)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }

    $scope.SortByTotalPR = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.pullRequest > b.pullRequest)
                return -1;
            if (a.pullRequest < b.pullRequest)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }

    $scope.SortByTotalPRAccepted = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.pullRequestAccepted > b.pullRequestAccepted)
                return -1;
            if (a.pullRequestAccepted < b.pullRequestAccepted)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }



    // Events
    angular.element(document).ready(function () {

    });

}]);

