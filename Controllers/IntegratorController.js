﻿'use strict';

/**
 * Contrôleur du module "Git Encouragement"
 */
app.controller('IntegratorController', ['$scope', '$http', 'GitHubService', function ($scope, $http, GitHubService) {

    // Variables
    $scope.contributors = [];
    $scope.repositoryUrl;
    $scope.emailNameSender;
    $scope.emailSender;
    $scope.emailText;
    $scope.endDate = new Date();
    $scope.startDate = new Date();
    $scope.isDisplayNonPullRequestContributor = true;
    $scope.isLoading = false;
    $scope.isAdvancedResearch = false;

    var NonPullRequestContributor = [];

    GitHubService.Initialize();

    // Public Functions
    $scope.BecomingIntegrator = function (id) {
        alert("Becoming contributor to " + id);
    }

    $scope.LoadRepository = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;

        var regexx = /(....)-(..)-(..)T/g;
        var ress = regexx.exec($scope.startDate.toISOString());
        var startDateMoment = moment(ress[1] + "-" + ress[2] + "-" + ress[3]);
        var regexx = /(....)-(..)-(..)T/g;
        var ress = regexx.exec($scope.endDate.toISOString());
        var endDateMoment = moment(ress[1] + "-" + ress[2] + "-" + ress[3]);

        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMoment).then(function () {
            $scope.DisplayNonPullRequestContributor();
            for (var i = 0; i < $scope.contributors.length; i++)
                $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);
            $scope.isLoading = false;
        });
    }

    $scope.SortByRatio = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.ratio > b.ratio)
                return -1;
            if (a.ratio < b.ratio)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }


    $scope.SortByRatioRevised = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.ratioRevised > b.ratioRevised)
                return -1;
            if (a.ratioRevised < b.ratioRevised)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }

    $scope.SortByTotalPR = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.pullRequest > b.pullRequest)
                return -1;
            if (a.pullRequest < b.pullRequest)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }

    $scope.SortByTotalPRAccepted = function () {
        $scope.contributors.sort(function (a, b) {
            if (a.pullRequestAccepted > b.pullRequestAccepted)
                return -1;
            if (a.pullRequestAccepted < b.pullRequestAccepted)
                return 1;
            // a doit être égale à b
            return 0;
        });
    }

    /*
    * This give the top 20 in function of the revised ratio
    */
    $scope.TopContributors = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;

        var startDateMoment = moment();
        var endDateMoment = moment().subtract(12, "months");
        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMoment).then(function () {
            $scope.DisplayNonPullRequestContributor();
            for (var i = 0; i < $scope.contributors.length; i++)
                $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);
            $scope.SortByRatioRevised()
            $scope.contributors.splice(20, $scope.contributors.length);
            $scope.isLoading = false;
        });
    }

    $scope.ExitContributors = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;

        var startDateMoment = moment().subtract(6, "months");
        var endDateMoment = moment().subtract(12, "months");
        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMoment).then(function () {
            $scope.DisplayNonPullRequestContributor();
            for (var i = 0; i < $scope.contributors.length; i++)
                $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);
            $scope.isLoading = false;
        });
    }

    $scope.DemotivatedContributors = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;

        var startDateMoment = moment().subtract(3, "months");
        var endDateMoment = moment().subtract(6, "months");
        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMoment).then(function () {
            $scope.DisplayNonPullRequestContributor();
            for (var i = 0; i < $scope.contributors.length; i++)
                $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);
            $scope.isLoading = false;
        });
    }

    $scope.ActifContributors = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;

        var startDateMoment = moment();
        var endDateMoment = moment().subtract(3, "months");
        GitHubService.GetContributors(repoOwner, repoName, $scope.contributors, startDateMoment, endDateMoment).then(function () {
            $scope.DisplayNonPullRequestContributor();
            for (var i = 0; i < $scope.contributors.length; i++)
                $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);
            $scope.isLoading = false;
        });
    }

    $scope.NewContributors = function () {
        $scope.isLoading = true;
        var splited = $scope.repositoryUrl.split('/');
        var repoName = splited.pop();
        var repoOwner = splited.pop();

        $scope.contributors = [];
        NonPullRequestContributor = [];
        $scope.isDisplayNonPullRequestContributor = true;

        var startDateMoment = moment();
        var endDateMoment = moment().subtract(3, "months");
        var startDateMoment2 = moment().subtract(3, "months");
        var endDateMoment2 = moment().subtract(12, "months");
        var newContributor = [];
        var oldContributor = [];
        GitHubService.GetContributors(repoOwner, repoName, newContributor, startDateMoment, endDateMoment).then(function () {
            GitHubService.GetContributors(repoOwner, repoName, oldContributor, startDateMoment2, endDateMoment2).then(function () {
                for (var i = 0; i < newContributor.length; i++) {
                    if (oldContributor[newContributor.login] != null)
                        newContributor.splice(i, 1);
                }

                $scope.contributors = newContributor;
                $scope.DisplayNonPullRequestContributor();

                for (var i = 0; i < $scope.contributors.length; i++)
                    $scope.contributors[i].dateLastPullRequestsString = $scope.DisplayDateMoment($scope.contributors[i].dateLastPullRequests);

                $scope.isLoading = false;

            });
        });
    }

    $scope.DisplayNonPullRequestContributor = function () {
        if ($scope.isDisplayNonPullRequestContributor) {
            for (var i = $scope.contributors.length - 1; i >= 0; i--) {
                if ($scope.contributors[i].pullRequest == 0) {
                    NonPullRequestContributor.push($scope.contributors.splice(i, 1)[0]);
                }
            }
            $scope.isDisplayNonPullRequestContributor = false;
        }
        else {
            for (var i = NonPullRequestContributor.length - 1; i >= 0; i--) {
                $scope.contributors.push(NonPullRequestContributor[i]);
            }
            NonPullRequestContributor = [];
            $scope.isDisplayNonPullRequestContributor = true;
        }
    }

    $scope.AdvancedResearch = function () {
        if ($scope.isAdvancedResearch) {

            $scope.isAdvancedResearch = false;
        }
        else {

            $scope.isAdvancedResearch = true;
        }
    }

    $scope.DisplayDateMoment = function (dateMoment) {
        if (dateMoment != null) {
            var regex = /(....)-(..)-(..)T/g;
            var res = regex.exec(dateMoment);
            return res[1] + "/" + res[2] + "/" + res[3];
        }
    }

    // Events
    angular.element(document).ready(function () {

    });

}]);


